﻿using System.Linq;
using UnityEngine;

public class PlayerAttack : MonoBehaviour, IMelee {
    
    private readonly float attackRange = 2f;

    public Transform attackPos;
    private int currentWeapon;
    private float timeBtwAttack;
    public LayerMask whatIsEnemies;

    public int Hit(int life) {
        var emptyGO = new GameObject();

        var newTransform = emptyGO.transform;

        newTransform.position = InitialPositionHelper.Get(transform.position.x + 1, transform.position.y + 0.3f, 0);

        var enemiesToDamage = Physics2D.OverlapCircleAll(newTransform.position, attackRange, whatIsEnemies);

        enemiesToDamage = enemiesToDamage.GroupBy(e => e.name).Select(e => e.First()).ToArray();

        var counter = 0;

        for (var i = 0; i < enemiesToDamage.Length; i++) {
            life--;
            counter++;
            enemiesToDamage[i].GetComponent<Obstacle>().TakeDamage();
            if (life == 0) break;
        }

        return counter;
    }
    
    // Start is called before the first frame update
    private void Update() {
        if (PlayerScore.playerScoreFormated.Equals(20)) CharacterMov.jumpForce = 13f;
    }

    private void OnDrawGizmosSelected() {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(attackPos.position, attackRange);
    }
}
﻿using UnityEngine;
using UnityEngine.UI;

public class SubMachineGun: MonoBehaviour, IWeapon {
    
    public Rigidbody2D rb;
    public float speed = 20f;

    public void Shoot() {
        ProjectileHelper.Execute("Bullet", transform, speed);
    }
    
}
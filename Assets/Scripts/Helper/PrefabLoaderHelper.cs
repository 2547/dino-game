﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrefabLoaderHelper : MonoBehaviour {

	public static GameObject Get(string path) {
		return Instantiate(Resources.Load("Prefab/" + path, typeof(GameObject))) as GameObject;
	}
	
}

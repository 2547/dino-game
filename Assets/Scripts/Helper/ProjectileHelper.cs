﻿using UnityEngine;

public class ProjectileHelper : MonoBehaviour {
    public static void Execute(string projectileName, Transform transform, float speed) {
        var initialPosition = new Vector3(transform.position.x + 1, transform.position.y + 0.3f, 0);
        var bullet = PrefabLoaderHelper.Get("Projectile");
        bullet.transform.position = initialPosition;
        bullet.GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>(projectileName);
        bullet.GetComponent<Rigidbody2D>().velocity = transform.right * speed;
    }
    
}
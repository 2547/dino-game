﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameObjectFinderHelper : MonoBehaviour {

	public static GameObject Get(string objectName) {
		return GameObject.Find(objectName);
	}
	
}

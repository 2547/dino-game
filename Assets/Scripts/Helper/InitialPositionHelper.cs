﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InitialPositionHelper : MonoBehaviour {

	public static Vector3 Get(float x, float y, float z) {
		return new Vector3(x, y, z);
	}
	
}

﻿using UnityEngine;
using UnityEngine.UI;

public class WeaponDisplay : MonoBehaviour {
    
    public void Load(string weapon, string desc) {

        GameObject image = GameObject.Find("Image");
        
        GameObject text = GameObject.Find("Desc");  
        
        text.GetComponent<Text>().text = desc;
      
        image.AddComponent(typeof(Image));
        image.GetComponent<Image>().sprite =  Resources.Load<Sprite>(weapon);
        
    }

}
﻿using UnityEngine;

public class CharacterMov : MonoBehaviour {
    public static float jumpForce = 9f;
    private bool canJump;
    private float forwardForce;

    private Rigidbody2D myBody;


    // Use this for initialization
    private void Start() {
        myBody = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    private void Update() {
        var canJump = Input.GetButtonDown("Jump");

        if (canJump) DoJump();
    }

    public void DoJump() {
        if (canJump) {
            canJump = false;
            forwardForce = 0f;
            myBody.velocity = new Vector2(forwardForce, jumpForce);
        }
    }

    private void OnCollisionEnter2D(Collision2D other) {
        canJump = true;
    }
}
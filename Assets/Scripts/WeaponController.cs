﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class WeaponController : MonoBehaviour {
    
    private readonly Dictionary<float, Weapon> weapons = new Dictionary<float, Weapon> {
        {50, new Weapon("Knife", 1, 3.0f, "Corta um cacto")},
        {100, new Weapon("Machete", 2, 3.0f, "Corta 2 cactos")},
        {150, new Weapon("Sword", 3, 3.0f, "Corta 3 cactos")},
        {200, new Weapon("Bow", 1, 3.0f, "Mata inimigos voadores")},
        {250, new Weapon("SubmachineGun", 1, 3.0f, "Sub Metralhadora")},
        {300, new Weapon("MachineGun", 1, 1.0f, "Metralhadora")},
        {350, new Weapon("Fire", 1, 1.0f, "Cuspir fogo")}
    };

    private float _currentWeapon;
    private Weapon _currentWeaponObject;
    private IMelee _iMelee;

    private IWeapon _iWeapon;
    private float timer;

    // Update is called once per frame
    private void Update() {
        timer += Time.deltaTime;
        
        // if (Input.GetButtonDown("Fire1"))  Execute<Fire>();
        
        // this.weapons[50];
        // uma faca == corta um cacto Knife 100
        // facao == 2 Machete 200
        // espada == 3 Sword 500
        // arco e flecha == matar os pássaros, volta a morrer para os cactos Bow 800
        // submetralhadora == matar os pássaros, quebrar um cacto SubmachineGun 1000
        // metralhadora == matar os pássaros, quebrar dois cactos MachineGun 1200
        // cuspir fogo == mata os pássaros e quebra os cactos Fire 1500
        switch (PlayerScore.playerScoreFormated) {
            case 50:
                _currentWeapon = PlayerScore.playerScoreFormated;
                _currentWeaponObject = weapons[_currentWeapon];
                _displayWeapon(_currentWeaponObject);
                break;
            case 100:
                _currentWeapon = PlayerScore.playerScoreFormated;
                _currentWeaponObject = weapons[_currentWeapon];
                _displayWeapon(_currentWeaponObject);
                break;
            case 150:
                _currentWeapon = PlayerScore.playerScoreFormated;
                _currentWeaponObject = weapons[_currentWeapon];
                _displayWeapon(_currentWeaponObject);
                break;
            case 200:
                _currentWeapon = PlayerScore.playerScoreFormated;
                _currentWeaponObject = weapons[_currentWeapon];
                _displayWeapon(_currentWeaponObject);
                break;
            case 250:
                _currentWeapon = PlayerScore.playerScoreFormated;
                _currentWeaponObject = weapons[_currentWeapon];
                _displayWeapon(_currentWeaponObject);
                break;
            case 300:
                _currentWeapon = PlayerScore.playerScoreFormated;
                _currentWeaponObject = weapons[_currentWeapon];
                _displayWeapon(_currentWeaponObject);
                break;
            case 350:
                _currentWeapon = PlayerScore.playerScoreFormated;
                _currentWeaponObject = weapons[_currentWeapon];
                _displayWeapon(_currentWeaponObject);
                break;
        }

        if (_currentWeaponObject.Life.Equals(0)) _displayWeapon(null);

        if (Input.GetButtonDown("Fire1")) ShootLogic();
    }

    private void ShootLogic() {
        if (_currentWeaponObject.Life > 0) Shoot(_currentWeaponObject.Name);
    }

    private void _displayWeapon(Weapon weapon) {
        try {
            gameObject.AddComponent<WeaponDisplay>().Load(weapon.Name, weapon.Desc);
        }
        catch (Exception e) {
            gameObject.AddComponent<WeaponDisplay>().Load(null, null);
        }
    }

    private bool isWeapons(string weapon) {
        if (!(weapon.Equals("Bow") || 
              weapon.Equals("SubmachineGun") || 
              weapon.Equals("MachineGun") || 
              weapon.Equals("Fire")
              )) {
            return true;
        }
        else {
            return false;
        }

    }

    private void Shoot(string weapon) {
        if (isWeapons(weapon)) {
            var character = GameObject.Find("Character");
            _iMelee = character.gameObject.GetComponent<PlayerAttack>();
            _currentWeaponObject.SubtractLife(_iMelee.Hit(_currentWeaponObject.Life));
        }
        else {
            switch (weapon) {
                case "Bow":
                    Execute<Bow>();
                    break;
                case "SubmachineGun":
                    Execute<SubMachineGun>();
                    break;
                case "MachineGun":
                    Execute<SubMachineGun>();
                    break;
                case "Fire":
                    Execute<Fire>();
                    break;
            }
        }
    }

    private void Execute<T>() where T : MonoBehaviour, IWeapon {
        
        if (timer > this._currentWeaponObject.Timer) {
            _iWeapon = gameObject.AddComponent<T>();
            _iWeapon.Shoot();
            timer = 0;
        }
        
    }
}
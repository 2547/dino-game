﻿using UnityEngine;

public class Bow : MonoBehaviour, IWeapon {
    
    public Rigidbody2D rb;
    public float speed = 20f;

    public void Shoot() {
        ProjectileHelper.Execute("Arrow", transform, speed);
    }
    
    private void Start() {
        rb.velocity = transform.right * speed;
    }
}
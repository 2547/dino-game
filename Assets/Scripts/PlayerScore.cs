﻿using UnityEngine;
using UnityEngine.UI;

public class PlayerScore : MonoBehaviour {
    
    public float playerScore;
    public static float playerScoreFormated;
    public Text scoreText;


    // Update is called once per frame
    private void Update() {
        playerScore += Time.deltaTime;
        playerScoreFormated =  float.Parse((playerScore * 10).ToString("0"));
        scoreText.text = playerScoreFormated.ToString("0");
    }

    private void OnDisable() {
        PlayerPrefs.SetInt("Score", (int) (playerScore * 10));
    }
}
﻿using UnityEngine;
using UnityEngine.UI;

public class Projectile : MonoBehaviour {
    
    private void OnTriggerEnter2D(Collider2D other) {

        if (this._GetWeaponName().Equals("Arrow")) {
            if (other.name.Contains("Fly")) {
                Destroy(other.gameObject);
                Destroy(gameObject);
            }
        }
        else {
            Destroy(other.gameObject);

            if (!_GetWeaponName().Equals("FireBall")) {
                Destroy(gameObject);
            }
            
        }
        
    }

    private string _GetWeaponName() {
        return gameObject.GetComponent<SpriteRenderer>().sprite.name;
    }

}
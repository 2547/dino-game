﻿public class Weapon {
    public Weapon(string name, int life, float timer, string desc) {
        Name = name;
        Life = life;
        Timer = timer;
        Desc = desc;
    }

    public string Name { get; set; }

    public int Life { get; set; }

    public float Timer { get; set; }
    
    public string Desc { get; set; }

    public void SubtractLife(int hits) {
        if (Life > 0) {
            Life = Life - hits;

            if (Life < 0) Life = 0;
        }
    }
}
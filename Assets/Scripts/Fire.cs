﻿using UnityEngine;
using UnityEngine.UI;

public class Fire: MonoBehaviour, IWeapon {
    
    public Rigidbody2D rb;
    public float speed = 20f;

    public void Shoot() {
        ProjectileHelper.Execute("FireBall", transform, speed);
    }
    
}
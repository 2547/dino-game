﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class Obstacle : MonoBehaviour {
    private readonly float speed = -6f;
    private Rigidbody2D myBody;

    // Use this for initialization
    private void Start() {
        myBody = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    private void Update() {
        myBody.velocity = new Vector2(speed, 0);
    }

    private void OnTriggerEnter2D(Collider2D other) {
        if (other.CompareTag("Player")) SceneManager.LoadScene(2);
    }

    public void TakeDamage() {
        Destroy(gameObject);
    }
}